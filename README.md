# Microfetch

## Current State

**fetch_accession_links.py**

Single script that will extract the requested IDs for a given species code and requested data type for a pre-selected set of fields. 
Output is written as a CSV to STDOUT.

Queries and return fields are currently hard-coded and selected by the dictionary keys. 
No filtering is currently done within the script

**fetch_taxon_links.py**

Used by fetch_accession_links.py to get the taxonomy links.

**extract_sample_data.py**

_Ignore this script, there's not really anything useful_
An older script that includes examples of filtering and data cleaning. 


However, we found getting experiment accessions ultimate returned fewer read files than extracting the run accessions. 
It may be that we need to extract sample+read+experiment and amalgamate them into a single set. 


## Basic filter for read_run
The read_run metadata contains the fields needed to determine if it's genome sequence and can potentially be assembled:
```
if row['library_strategy'] == 'WGS' \
   or row['instrument_platform'] == 'ILLUMINA' \
   or row['library_layout'] == 'PAIRED' \
   or int(row['base_count']) > taxon_minimum:
```

## Example commands
Usage:
```
# fetch_accession_links.py --help
Usage: fetch_accession_links.py [OPTIONS]

Options:
  -t, --taxon_id TEXT        NCBI ID. Will also fetch the subtree.  [required]
  -a, --accession-type TEXT  experiment (default), sample, run
  -r, --result-type TEXT     read_run, read_experiment, sample
  -o, --offset INTEGER       Continue a partial download.
  -l, --limit INTEGER        Change limit to number of requests
  -q, --query TEXT           all/identifiers
  --help                     Show this message and exit.
```

Fetch all the Klebsiella-linked run_accessions and all read_run metadata
``````
# python3 fetch_accession_links -t 570 --accession-type run --result-type read_run -q all
```

